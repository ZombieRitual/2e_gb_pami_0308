import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "App motos";

  cards = [
  {
    titulo: "Moto 1",
    subtitulo: "Harley-Davidson Fat Boy R$ 65.400,00",
    conteudo: "A Harley-Davidson Fat Boy é uma motocicleta cruiser softail V-twin com rodas de disco fundido sólido.",
    foto: "https://s2.glbimg.com/IqDPRtJPyEIBypf29TUj9nMEup4=/smart/e.glbimg.com/og/ed/f/original/2020/02/11/harley-davidson-fat-boy-30th-anniversary.jpg"
  },
  {
    titulo: "Moto 2",
    subtitulo: "BMW R 18 R$ 136.000,00 ",
    conteudo: "BMW R 18 é uma motocicleta cruiser fabricada pela BMW Motorrad.",
    foto: "https://cdn.motor1.com/images/mgl/VmZXR/s1/2021-bmw-r-18.jpg"
  },
  {
    titulo: "Moto 3",
    subtitulo: "Kawasaki Ninja 400 R$ 74.990,00",
    conteudo: "A Kawasaki Ninja 400 é uma moto esportiva da série Ninja de 399 cc.",
    foto: "https://s2.glbimg.com/hvyzsMCyjKzGyUiFm7flVsRJ_1w=/0x0:1900x1267/600x0/smart/filters:gifv():strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2018/1/H/MZaoYWQmmqH4AJCFq5mQ/kawasaki-ninja-400-q98a2289-credito-marcelo-brandt-g1.jpg"
  },
  {
    titulo: "Moto 4",
    subtitulo: "Yamaha MT-09 R$ 45.990,00",
    conteudo: "A Yamaha MT-09 é uma motocicleta Yamaha nua ou padrão da série MT com um motor DOHC em linha de 847-890 cc.",
    foto: "https://static.wixstatic.com/media/bb585c_6da35b47acce4b3888668455602b2f0b~mv2.jpg/v1/fill/w_1000,h_667,al_c,q_90/bb585c_6da35b47acce4b3888668455602b2f0b~mv2.jpg"
  }
];
 
  constructor() {}

}
